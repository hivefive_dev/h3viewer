
#include <QFile>
#include <QMouseEvent>
#include <QApplication>
#include <QStyle>
#include <QtMessageHandler>

#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "memoryscanner.h"

#define SETTINGS_WINDOW_X "windowX"
#define SETTINGS_WINDOW_Y "windowY"
#define SETTINGS_AUTO_HIDE_INTERACTIONS_ENABLED "autoHideEnabled"
#define SETTINGS_AUTO_HIDE_NEW_DAY_ENABLED "autoHideNewDayEnabled"
#define SETTINGS_AUTO_HIDE_TIME "autoHideTime"

#define NONE "None"

static AboutWindow *globalAboutWindow = {NULL};
static bool debugActivated = {false};

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)
    QByteArray localMsg = msg.toLocal8Bit();
    if (globalAboutWindow == NULL)
    {
        return;
    }
    switch (type) {
    case QtDebugMsg:
        if (debugActivated)
        {
            globalAboutWindow->addToLog("Debug: " + msg);
            fprintf(stdout, "Debug: %s\n", localMsg.constData());
        }
        break;
    case QtInfoMsg:
        globalAboutWindow->addToLog("Info: " + msg);
        fprintf(stdout, "Info: %s\n", localMsg.constData());
        break;
    case QtWarningMsg:
        globalAboutWindow->addToLog("Warning: " + msg);
        fprintf(stderr, "Warning: %s\n", localMsg.constData());
        break;
    case QtCriticalMsg:
        globalAboutWindow->addToLog("Critical: " + msg);
        fprintf(stderr, "Critical: %s\n", localMsg.constData());
        break;
    case QtFatalMsg:
        globalAboutWindow->addToLog("Fatal: " + msg);
        fprintf(stderr, "Fatal: %s\n", localMsg.constData());
        break;
    }
    std::cout << std::flush;
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      hideToggle(true),
      holdingLeft(false),
      about(this),
      settings(this),
      storedSettings("HF:THV","HF:THV")
{
    ui->setupUi(this);

    connect(this->hero.getMemoryScannerObject(), &MemoryScanner::dataUpdated, this, &MainWindow::refreshUI);

    connect(this->ui->actionSettings, &QAction::triggered, &this->settings, &SettingsWindow::show);
    connect(this->ui->actionAbout, &QAction::triggered, &this->about, &AboutWindow::show);
    connect(this->ui->actionExit, &QAction::triggered, this, &MainWindow::exit);

    connect(&(this->settings), &SettingsWindow::includeDebugUpdated, this, &MainWindow::updateDebugStatus);


    globalAboutWindow = &(this->about);
    qInstallMessageHandler(myMessageOutput);

    toggleView();
    loadSettings();
    initHeroMapper();
}


void MainWindow::loadSettings()
{
    if(this->storedSettings.contains(SETTINGS_WINDOW_X))
    {
        move(this->storedSettings.value(SETTINGS_WINDOW_X).toInt(), y());
    }
    if(this->storedSettings.contains(SETTINGS_WINDOW_Y))
    {
        move(x(), this->storedSettings.value(SETTINGS_WINDOW_Y).toInt());
    }
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}

void MainWindow::initHeroMapper()
{
    this->hero.init();
}


QString styleSheeting(QString item)
{
    if (item == NONE || item.isEmpty()) {
        return QString("background-color: transparent;");
    }

    QString itemType = "spells";
    if(item.contains("Artifact_"))
    {
        itemType = "items";
    }
    if(item.contains("Skill_"))
    {
        itemType = "skills";
    }
    if(item.contains("Hero_"))
    {
        itemType = "heroes";
    }
    if(item.contains("Monster_"))
    {
        itemType = "monsters";
    }

    QFile test(":/images/" + itemType + "/" + item);
    if (test.exists())
    {
        //return QString("border-image: url(:/images/" + itemType + "/" + item + ") 1 1 1 1 stretch stretch;");
        return QString("background-image: url(:/images/" + itemType + "/" + item + ");");
    }
    else
    {
        qDebug() << "Item image does not exist: " << itemType + "/" + item;
        return QString("Unknown item!");
    }
}

void MainWindow::clearModfiers()
{
    ui->attackLabel->setText("0");
    ui->DefLabel->setText("0");
    ui->powerLabel->setText("0");
    ui->knowLabel->setText("0");
}

void MainWindow::refreshUI()
{
    ui->attackLabel->setText(hero.getAttack());
    ui->DefLabel->setText(hero.getDefence());
    ui->powerLabel->setText(hero.getPower());
    ui->knowLabel->setText(hero.getKnowledge());

    ui->heroNameLabel->setText(hero.getHeroName());
    ui->heroPotraitLabel->setStyleSheet(styleSheeting(hero.getHeroPotrait()));
    ui->levelLabel->setText(hero.getLevel());
    ui->spLabel->setText(hero.getSP());

    ui->spellBookLabel->setStyleSheet(styleSheeting(hero.getSpellBook()));

    ui->balistaLabel->setStyleSheet(styleSheeting(hero.getBalista()));
    ui->firstAidTentLabel->setStyleSheet(styleSheeting(hero.getFirstAidTent()));
    ui->amoCartLabel->setStyleSheet(styleSheeting(hero.getAmmoCart()));

    ui->helmetLabel->setStyleSheet(styleSheeting(hero.getHelmet()));
    ui->capeLabel->setStyleSheet(styleSheeting(hero.getCape()));
    ui->neckLabel->setStyleSheet(styleSheeting(hero.getNeck()));
    ui->WeaponLabel->setStyleSheet(styleSheeting(hero.getWeapon()));
    ui->shieldLabel->setStyleSheet(styleSheeting(hero.getSheild()));
    ui->bodyLabel->setStyleSheet(styleSheeting(hero.getChest()));
    ui->ringLeftLabel->setStyleSheet(styleSheeting(hero.getRingLeft()));
    ui->ringRightLabel->setStyleSheet(styleSheeting(hero.getRingRight()));
    ui->feetLabel->setStyleSheet(styleSheeting(hero.getBoots()));
    ui->arti1Label->setStyleSheet(styleSheeting(hero.getArti0()));
    ui->arti2Label->setStyleSheet(styleSheeting(hero.getArti1()));
    ui->arti3Label->setStyleSheet(styleSheeting(hero.getArti2()));
    ui->arti4Label->setStyleSheet(styleSheeting(hero.getArti3()));
    ui->arti5Label->setStyleSheet(styleSheeting(hero.getArti4()));

    ui->skillSlot0Label->setStyleSheet(styleSheeting(hero.getSkill0()));
    ui->skillSlot1Label->setStyleSheet(styleSheeting(hero.getSkill1()));
    ui->skillSlot2Label->setStyleSheet(styleSheeting(hero.getSkill2()));
    ui->skillSlot3Label->setStyleSheet(styleSheeting(hero.getSkill3()));
    ui->skillSlot4Label->setStyleSheet(styleSheeting(hero.getSkill4()));
    ui->skillSlot5Label->setStyleSheet(styleSheeting(hero.getSkill5()));
    ui->skillSlot6Label->setStyleSheet(styleSheeting(hero.getSkill6()));
    ui->skillSlot7Label->setStyleSheet(styleSheeting(hero.getSkill7()));

    ui->creature0Frame->setStyleSheet("#creature0Frame{" + styleSheeting(hero.getCreature0()) +"}");
    ui->creature1Frame->setStyleSheet("#creature1Frame{" + styleSheeting(hero.getCreature1()) +"}");
    ui->creature2Frame->setStyleSheet("#creature2Frame{" + styleSheeting(hero.getCreature2()) +"}");
    ui->creature3Frame->setStyleSheet("#creature3Frame{" + styleSheeting(hero.getCreature3()) +"}");
    ui->creature4Frame->setStyleSheet("#creature4Frame{" + styleSheeting(hero.getCreature4()) +"}");
    ui->creature5Frame->setStyleSheet("#creature5Frame{" + styleSheeting(hero.getCreature5()) +"}");
    ui->creature6Frame->setStyleSheet("#creature6Frame{" + styleSheeting(hero.getCreature6()) +"}");

    ui->creatureAmount0Label->setText(hero.getCreatureAmount0());
    ui->creatureAmount1Label->setText(hero.getCreatureAmount1());
    ui->creatureAmount2Label->setText(hero.getCreatureAmount2());
    ui->creatureAmount3Label->setText(hero.getCreatureAmount3());
    ui->creatureAmount4Label->setText(hero.getCreatureAmount4());
    ui->creatureAmount5Label->setText(hero.getCreatureAmount5());
    ui->creatureAmount6Label->setText(hero.getCreatureAmount6());

    this->previosAttack = hero.getAttack();
    this->previosDefence = hero.getDefence();
    this->previosPower = hero.getPower();
    this->previosKnowledge = hero.getKnowledge();
}

void MainWindow::updateDebugStatus(const bool &activated)
{
    debugActivated = activated;
}

void MainWindow::toggleView()
{
    if(this->hideToggle)
    {
        ui->mainFrame->setStyleSheet("#mainFrame{background: transparent;} QLabel{border: 1px solid #D3C69A;}");
        setAttribute(Qt::WA_TranslucentBackground);

        QWidget::setWindowFlags( Qt::Window|
                                 Qt::FramelessWindowHint|
                                 Qt::WindowSystemMenuHint|
                                 Qt::CustomizeWindowHint);

        QWidget::setGeometry(QWidget::geometry().x() + 1,
                             QWidget::geometry().y() +
                             QApplication::style()->pixelMetric(QStyle::PM_TitleBarHeight) +
                             ui->menuBar->height(),
                             ui->mainFrame->geometry().width(),
                             ui->mainFrame->geometry().height());
        ui->menuBar->hide();
    }
    else
    {
        ui->mainFrame->setStyleSheet("#mainFrame{background: url(:/images/HeroBackground.png);} QLabel{border: 1px solid #D3C69A;}");
        setAttribute(Qt::WA_TranslucentBackground, false);

        QWidget::setWindowFlags( Qt::Window|
                                 Qt::WindowTitleHint|
                                 Qt::WindowSystemMenuHint|
                                 Qt::WindowMinMaxButtonsHint|
                                 Qt::WindowCloseButtonHint|
                                 Qt::WindowFullscreenButtonHint);
        ui->menuBar->show();


        QWidget::setGeometry(QWidget::geometry().x(),
                             QWidget::geometry().y() -
                             QApplication::style()->pixelMetric(QStyle::PM_TitleBarHeight) -
                             ui->menuBar->height() + 9,
                             ui->mainFrame->geometry().width(),
                             ui->mainFrame->geometry().height() + 22);

    }

    this->hideToggle = !(this->hideToggle);
    QWidget::show();
}

void MainWindow::mousePressEvent(QMouseEvent* event)
{

    if (event->button() == Qt::RightButton)
    {
        toggleView();
    }

    if (event->button() == Qt::LeftButton )
    {
        this->dragingOldWindowPos = event->globalPosition();
        this->holdingLeft = true;
    }

}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton )
    {
        this->holdingLeft = false;
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (this->holdingLeft)
    {
        const QPointF delta = event->globalPosition() - this->dragingOldWindowPos;
        move(x()+delta.x(), y()+delta.y());
        this->dragingOldWindowPos = event->globalPosition();
    }
}


void MainWindow::saveSettings()
{
    this->storedSettings.setValue(SETTINGS_WINDOW_X, x());
    this->storedSettings.setValue(SETTINGS_WINDOW_Y, y());
    this->storedSettings.sync();
}

void MainWindow::exit()
{
    QApplication::exit(0);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
    exit();
}

