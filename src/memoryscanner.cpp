#include "memoryscanner.h"

#include <tlhelp32.h>
#include <psapi.h>
#include <wchar.h>

#include <QObject>
#include <QDebug>

#define HERO_ALLOCATION_SIZE 1170

#define HOMM_PROCESS_NAME L"Heroes3.exe"
#define HOMM_HD_PROCESS_NAME (wchar_t*)L"Heroes3 HD.exe"

#define SOD_DLL_FILE (wchar_t*)L"HD_SOD.dll"
#define SOD_DLL_TO_PLAYERS_OFFSET 0x135c38
#define SOD_PLAYERS_TO_HEROES_OFFSET 0xB50

#define HOTA_PROCESS_NAME L"h3hota.exe"
#define HOTA_HD_PROCESS_NAME (wchar_t*)L"h3hota HD.exe"

#define HOTA_DLL_FILE (wchar_t*)L"hota.dll"
#define HOTA_DLL_TO_PLAYERS_OFFSET 0x190160  // This might change when the game is updated
#define HOTA_DLL_TO_HEROES_OFFSET 0x1913B8   // This might change when the game is updated

#define EXE_TO_UNKNOWN_OFFSET 0x2992B8 // This might change when the game is updated
#define UNKNOWN_TO_BOTTOM_RIGHT_STATUS_ID_OFFSET 0x394
#define UNKNOWN_TO_ACTIVITY_OFFSET 0x39C

#define PLAYER_TO_SELECTED_HERO_OFFSET 0x04
#define PLAYER_TO_LOCAL_PLAYER_OFFSET 0xE1


MemoryScanner::MemoryScanner():
    QObject(NULL),
    processHandler(NULL),
    heroID(0),
    selectedHeroAddress(NULL),
    dllBaseAddress(0),
    h3BaseAddress(0),
    heroBuffer(NULL),
    previosHeroBuffer(NULL),
    previousBRStatusID(0),
    gameIsSoD(false)

{
    this->pollRateTimer.setInterval(50);
    this->processCheckTimer.setInterval(2000);
    connect( &(this->pollRateTimer), &QTimer::timeout, this, &MemoryScanner::updateState);
    connect( &(this->processCheckTimer), &QTimer::timeout, this, &MemoryScanner::attachToProcess);

}


bool MemoryScanner::init(uint8_t *heroBuffer, const size_t &bufferSize)
{
    if (bufferSize < HERO_ALLOCATION_SIZE)
    {
        qWarning() << "Too small buffer size for hero buffer.";
        return false;
    }
    this->heroBuffer = heroBuffer;

    if(!(this->previosHeroBuffer))
    {
        this->previosHeroBuffer = new uint8_t[HERO_ALLOCATION_SIZE];
    }
    this->pollRateTimer.start();
    this->processCheckTimer.start();
    this->attachToProcess();
    qDebug() << "Scannar initialized";
    return true;
}

uint32_t MemoryScanner::getHeroID()
{
    return this->heroID;
}

uintptr_t MemoryScanner::getPointerValue(const uintptr_t &address)
{
    uintptr_t pointerValue = 0;
    size_t bytesRead = 0;
    qDebug() << (LPCVOID)(address) << " - Pointer value - address";
    if(!ReadProcessMemory(this->processHandler,
                          (LPCVOID)(address),
                          &pointerValue,
                          sizeof(uint32_t),
                          &bytesRead))
    {
        DWORD lastError = GetLastError();
        if(lastError != 299)
            qWarning() << "Could not read ponter at " << address <<  " . Error code: " << lastError;
        return 0;
    }
    qDebug() << pointerValue << " - Pointer value";
    return pointerValue;
}

DWORD MemoryScanner::GetHommPid()
{
    qDebug() << "Getting HoMM PID.";

    PROCESSENTRY32 pe32;
    HANDLE hSnapshot = NULL;
    pe32.dwSize = sizeof(PROCESSENTRY32);
    hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    DWORD pid = 0;

    if(Process32First( hSnapshot, &pe32))
    {
        do
        {
            if(wcscmp( pe32.szExeFile, HOTA_PROCESS_NAME) == 0 ||
                    wcscmp( pe32.szExeFile, HOTA_HD_PROCESS_NAME) == 0)
            {
                this->gameIsSoD = false;
                pid = pe32.th32ProcessID;
                break;
            }

            if(wcscmp( pe32.szExeFile, HOMM_PROCESS_NAME) == 0 ||
                    wcscmp( pe32.szExeFile, HOMM_HD_PROCESS_NAME) == 0)
            {
                this->gameIsSoD = true;
                pid = pe32.th32ProcessID;
                break;
            }
        } while( Process32Next(hSnapshot, &pe32));
    }

    if(hSnapshot != INVALID_HANDLE_VALUE)
    {
        CloseHandle(hSnapshot);
    }

    return pid;
}


uintptr_t GetModuleBaseAddress(const DWORD &dwProcID, wchar_t * moduleName)
{
    qDebug() << "Gettings module offset address.";
    uintptr_t ModuleBaseAddress = 0;
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, dwProcID);
    if (hSnapshot != INVALID_HANDLE_VALUE)
    {
        MODULEENTRY32 ModuleEntry32;
        ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
        if (Module32First(hSnapshot, &ModuleEntry32))
        {
            do
            {
                if (wcscmp(ModuleEntry32.szModule, moduleName) == 0)
                {
                    ModuleBaseAddress = (uintptr_t)ModuleEntry32.modBaseAddr;
                    break;
                }
            } while (Module32Next(hSnapshot, &ModuleEntry32));
        }
        CloseHandle(hSnapshot);
    }
    return ModuleBaseAddress;
}


bool MemoryScanner::giveExtraPermissions()
{
    qDebug() << "Asking for elevated permissions";
    bool bRtn = false;
    TOKEN_PRIVILEGES tp;
    HANDLE tokenhandle = NULL;
    HANDLE ProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessId());
    if (!ProcessHandle)
    {
        DWORD lastError = GetLastError();
        qWarning() << "Could not open own process to set permissions. Error code: " << lastError;
        return false;
    }

    bRtn = OpenProcessToken(ProcessHandle, TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES, &tokenhandle);
    if (bRtn == 0)
    {
        DWORD lastError = GetLastError();
        qWarning() << "Could not open process tokens. Error code: " << lastError;
        return false;
    }

    bRtn = LookupPrivilegeValueW(NULL, SE_DEBUG_NAME, &(tp.Privileges[0].Luid));
    if (bRtn)
    {
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
        tp.PrivilegeCount = 1;
        bRtn = AdjustTokenPrivileges(tokenhandle, false, &tp, sizeof(tp), NULL, NULL);
        if(!bRtn)
        {
            DWORD lastError = GetLastError();
            qWarning() << "Could not adjust privilages for this process. Error code: " << lastError;
            return false;
        }
    }
    else
    {
        DWORD lastError = GetLastError();
        qWarning() << "Could not look up privilage for this process. Error code: " << lastError;
        return false;
    }
    return true;
}


bool MemoryScanner::attachToProcess()
{
    this->HommPID = GetHommPid();
    if (!this->HommPID)
    {
        qDebug() << "No HoMM pid...";
        // If no pid was found, clearing hero buffer and other stuff
        this->processHandler = NULL;
        this->selectedHeroAddress = NULL;
        this->dllBaseAddress = 0;
        this->h3BaseAddress = 0;
        this->heroID = 0;
        if (this->heroBuffer != NULL)
        {
            memset(this->heroBuffer, 0, HERO_ALLOCATION_SIZE);
            emit dataUpdated();
        }
        return false;
    }

    if(!this->processHandler)
    {
        // Extra permissions is required for this process to be
        // allowed to attach to HoMM HotA process
        giveExtraPermissions();

        qDebug() << "Trying to attatch to HoMM...";
        this->processHandler = OpenProcess(PROCESS_VM_READ, false, this->HommPID);
        if (!this->processHandler)
        {
            DWORD errorCode = GetLastError();
            qWarning() << "Failed to attach to process, error code: " << errorCode;
            return false;
        }
        qDebug() << "Attached to HoMM3, pid: " << this->HommPID;
    }
    return true;
}


bool MemoryScanner::findSelectedHero()
{
    size_t bytesRead = 0;
    size_t selectedHeroID = 0;
    uint32_t playerAddress = 0;
    uint32_t heroSectionAddress = 0;
    uint8_t isPlayerLocal = 0;

    if(!this->processHandler)
    {
        return false;
    }

    if(!this->gameIsSoD && this->dllBaseAddress == 0)
    {
        // Gets the address offset of the hota.dll in memory.
        this->dllBaseAddress = GetModuleBaseAddress(this->HommPID, HOTA_DLL_FILE);
    }
    if(this->gameIsSoD && this->dllBaseAddress == 0)
    {
        // Gets the address offset of the hota.dll in memory.
        this->dllBaseAddress = GetModuleBaseAddress(this->HommPID, SOD_DLL_FILE);
    }


    // Checks for a ponter on a specific offset from the hota base address. It
    // points to the active player's struct in memory.
    // This might break when the game is updated.
    if (this->gameIsSoD)
    {
        playerAddress = getPointerValue((this->dllBaseAddress + SOD_DLL_TO_PLAYERS_OFFSET));
    }
    else
    {
        playerAddress = getPointerValue((this->dllBaseAddress + HOTA_DLL_TO_PLAYERS_OFFSET));
    }

    if(!playerAddress)
    {
        return false;
    }

    qDebug() << "Player address: " << (LPCVOID)(intptr_t)playerAddress;

    // playerAddress contains a pointer to the active player's "player struct"
    // The selected hero id is 4 bytes into the "player struct"
    selectedHeroID = getPointerValue((playerAddress + PLAYER_TO_SELECTED_HERO_OFFSET));

    qDebug() << "Hero ID: " << selectedHeroID;

    // In the player section, each player have a bit if the player is local
    // And another bit if the player is remote (online play). Checking if
    // the local bit is set.
    if(!ReadProcessMemory(this->processHandler,
                          (LPCVOID)(intptr_t)(playerAddress + PLAYER_TO_LOCAL_PLAYER_OFFSET),
                          &isPlayerLocal,
                          sizeof(isPlayerLocal),
                          &bytesRead))
    {   
        DWORD lastError = GetLastError();
        qWarning() << "Could not read Player name. Error code: " << lastError;
        return false;
    }

    // If "local player" bit is not set, we should not read the hero data,
    // that would be cheating!!!
    if(!isPlayerLocal)
    {
        return false;
    }


    // Checks for a ponter on a specific offset from the hota base address. It
    // points to the hero section where ALL heroes are in memory.
    // This might break when the game is updated.
    if (!this->gameIsSoD)
    {
        heroSectionAddress = getPointerValue((this->dllBaseAddress + HOTA_DLL_TO_HEROES_OFFSET));
    }
    else
    {
        heroSectionAddress = getPointerValue(this->dllBaseAddress + SOD_DLL_TO_PLAYERS_OFFSET)  + SOD_PLAYERS_TO_HEROES_OFFSET;
    }

    if(heroSectionAddress == 0)
    {
        return false;
    }

    qDebug() << "Hero section address: " << heroSectionAddress;

    // 0xFFFFFFFF is the default value, aka no hero selected
    if (selectedHeroID != 0xFFFFFFFF)
    {
        this->heroID = selectedHeroID;
        // Get the offset from the hero section to the selected hero
        this->selectedHeroAddress = (LPCVOID)(intptr_t)(heroSectionAddress + HERO_ALLOCATION_SIZE * this->heroID);
        qDebug() << "The selected hero is at address " << this->selectedHeroAddress;
        return true;
    }
    return false;
}


bool MemoryScanner::copySelectedHeroFromMemory()
{
    if (this->processHandler == 0 ||
            this->selectedHeroAddress == NULL ||
            this->heroBuffer == NULL)
    {
        return false;
    }

    size_t bytesRead = 0;

    if (ReadProcessMemory(this->processHandler,
                          this->selectedHeroAddress,
                          this->heroBuffer,
                          HERO_ALLOCATION_SIZE,
                          &bytesRead))
    {
        if(bytesRead != HERO_ALLOCATION_SIZE)
        {
            qWarning() << "Only read " << bytesRead << "bytes, expected " << HERO_ALLOCATION_SIZE << "bytes";
        }
        qDebug() << "Copied the hero info, " << bytesRead << " bytes copied from the game";
        qDebug() << this->heroBuffer;
        return true;
    }
    else
    {
        DWORD lastError = GetLastError();
        qWarning() << "Error trying to scan Hero data. Error code: " << lastError;
    }
    return false;
}


void MemoryScanner::checkForMapInteractions()
{

    if(!this->gameIsSoD && this->h3BaseAddress == 0)
    {
        this->h3BaseAddress = GetModuleBaseAddress(GetHommPid(), HOTA_HD_PROCESS_NAME);
    }
    if(this->gameIsSoD && this->h3BaseAddress == 0)
    {
        this->h3BaseAddress = GetModuleBaseAddress(GetHommPid(), HOMM_HD_PROCESS_NAME);
    }

    size_t bytesRead;
    uint8_t bottomRightStatusId;
    uint16_t interactionID;

    // Gets a pointer which points to a unmapped struct, but it contains
    // values which are useful
    uint32_t unknownStruktPnt = getPointerValue(this->h3BaseAddress + EXE_TO_UNKNOWN_OFFSET);
    if(unknownStruktPnt == 0)
    {
        return;
    }

    // This byte contains the ID of what type of image/animation should appear
    // in the bottom right square, things like sun rise animation or
    // interacting with map object. "Normal" state is value 3, interaction is
    // value 6.
    ReadProcessMemory(this->processHandler,
                      (LPCVOID)(intptr_t)(unknownStruktPnt + UNKNOWN_TO_BOTTOM_RIGHT_STATUS_ID_OFFSET),
                      &bottomRightStatusId,
                      sizeof(uint8_t),
                      &bytesRead);

    // Another slightly unknown value, but it seems to be updated at convinent
    // times. For example, picking up two objects at close to each other will
    // make the above value not change (stay at 6 for both pickups) while this
    // value will change for each pickup.
    ReadProcessMemory(this->processHandler,
                      (LPCVOID)(intptr_t)(unknownStruktPnt + UNKNOWN_TO_ACTIVITY_OFFSET),
                      &interactionID,
                      sizeof(uint16_t),
                      &bytesRead);

    qDebug() << "Bottom right status id: " << bottomRightStatusId;

    // Checking if we have activated a pickup/interaction (value 6)
    if (bottomRightStatusId == 6 &&
            (previousBRStatusID != bottomRightStatusId || previousInteractionID != interactionID))
    {
        emit actionViewUpdated(itemPickup);
    }
    // ID 1 = new day
    if (bottomRightStatusId == 1 && previousBRStatusID != bottomRightStatusId)
    {
        emit actionViewUpdated(newDay);
    }
    // ID 3 = default/army veiw
    if (bottomRightStatusId == 3)
    {
        emit actionViewUpdated(restore);
    }
    previousBRStatusID = bottomRightStatusId;
    previousInteractionID = interactionID;
    return;
}


void MemoryScanner::updateState()
{
    if(!findSelectedHero())
    {
        return;
    }
    if(!copySelectedHeroFromMemory())
    {
        return;
    }

    checkForMapInteractions();

    // If the memory is unchanged, don't send a signal that data was updated.
    if(memcmp(heroBuffer, previosHeroBuffer, HERO_ALLOCATION_SIZE) == 0)
    {
        return;
    }
    else
    {
        qDebug() << heroBuffer;
        memcpy(previosHeroBuffer, heroBuffer, HERO_ALLOCATION_SIZE);
        emit dataUpdated();
    }
}

