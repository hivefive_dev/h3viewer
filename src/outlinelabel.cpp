#include "outlinelabel.h"

OutlineLabel::OutlineLabel(QWidget *parent)
    :QLabel(parent),
      once(false)
{
    outlinePen.setWidth(5);
    outlinePen.setColor(Qt::black);
}

void OutlineLabel::paintEvent(QPaintEvent *event)
{

    // Not sure why this can't be done in the constructor
    if(!once)
    {
        this->outlineFont.setFamily(this->font().family());
        this->outlineFont.setPointSize(this->font().pointSize());
        this->outlineFont.setWeight(QFont::Thin);
        this->outlineFont.setBold(true);
        once = true;
    }

    QPainter painter(this);

    // Clear the last outline drawing
    outlinePath.clear();

    QFontMetrics fm(this->font());
    int textWide = fm.horizontalAdvance(this->text());
    int textHeight = fm.height();
    int textX = 0;
    int textY = 0;

    // These are just tested into OK values. Might be a better way to align this
    if ((this->alignment() & Qt::AlignLeft) == Qt::AlignLeft)
        textX = 0;
    if ((this->alignment() & Qt::AlignHCenter) == Qt::AlignHCenter)
        textX = ( this->width() - textWide) / 2;
    if ((this->alignment() & Qt::AlignRight) == Qt::AlignRight)
        textX = this->width() - textWide ;

    if ((this->alignment() & Qt::AlignTop) == Qt::AlignTop)
        textY = textHeight - this->font().pointSize()/4;
    if ((this->alignment() & Qt::AlignVCenter) == Qt::AlignVCenter)
        textY =  ( this->height() + textHeight) / 2  - this->font().pointSize()/4;
    if ((this->alignment() & Qt::AlignBottom) == Qt::AlignBottom)
        textY = this->height() - this->font().pointSize()/4;

    painter.setFont(outlineFont);
    painter.setPen(outlinePen);


    outlinePath.addText(textX, textY , outlineFont, this->text());

    // Draw the outline
    painter.drawPath(outlinePath);

    // Draw the "normal" text
    QLabel::paintEvent(event);
}
