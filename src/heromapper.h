#ifndef HEROMAPPER_H
#define HEROMAPPER_H

#include <QObject>

#include "imagesMap.h"
#include "memoryscanner.h"


#pragma pack(1)
struct HeroStruct
{                                                    // Byte offset:
    uint16_t posX;                                   // 0
    uint16_t posY;                                   // 2
    uint8_t padding1[20];                            // 4
    uint16_t mana;                                   // 24
    uint8_t padding2[9];                             // 26
    char heroName[13];                               // 35
    uint8_t padding3[4];                             // 48
    uint32_t plannedMoveX; /** Might be wrong place*/// 52
    uint32_t plannedMoveY; /** Might be wrong place*/// 56
    uint8_t padding4[25];                            // 60
    uint8_t level;                                   // 85
    uint8_t padding5[59];                            // 86
    uint32_t creatureIDInSlot0;                      // 145
    uint32_t creatureIDInSlot1;                      // 149
    uint32_t creatureIDInSlot2;                      // 153
    uint32_t creatureIDInSlot3;                      // 157
    uint32_t creatureIDInSlot4;                      // 161
    uint32_t creatureIDInSlot5;                      // 165
    uint32_t creatureIDInSlot6;                      // 169
    uint32_t creatureAmountInSlot0;                  // 173
    uint32_t creatureAmountInSlot1;                  // 177
    uint32_t creatureAmountInSlot2;                  // 181
    uint32_t creatureAmountInSlot3;                  // 185
    uint32_t creatureAmountInSlot4;                  // 189
    uint32_t creatureAmountInSlot5;                  // 193
    uint32_t creatureAmountInSlot6;                  // 197
    uint8_t pathfindingSkill;                        // 201
    uint8_t archerySkill;                            // 202
    uint8_t logisticSkill;                           // 203
    uint8_t scoutingSkill;                           // 204
    uint8_t diplomacySkill;                          // 205
    uint8_t navigationSkill;                         // 206
    uint8_t leadershipSkill;                         // 207
    uint8_t wisdomSkill;                             // 208
    uint8_t mysticismSkill;                          // 209
    uint8_t luckSkill;                               // 210
    uint8_t ballisticsSkill;                         // 211
    uint8_t egleEyeSkill;                            // 212
    uint8_t necromancySkill;                         // 213
    uint8_t estateSkill;                             // 214
    uint8_t fireMagicSkill;                          // 215
    uint8_t airMagicSkill;                           // 216
    uint8_t waterMagicSkill;                         // 217
    uint8_t earthMagicSkill;                         // 218
    uint8_t scholarSkill;                            // 219
    uint8_t tacticsSkill;                            // 220
    uint8_t artillerySkill;                          // 221
    uint8_t learningSkill;                           // 222
    uint8_t offenceSkill;                            // 223
    uint8_t armorerSkill;                            // 224
    uint8_t intelligenceSkill;                       // 225
    uint8_t sorcerySkill;                            // 226
    uint8_t resistanceSkill;                         // 227
    uint8_t firstAidSkill;                           // 228
    uint8_t interferenceSkill;                       // 229
    uint8_t padding6[71];                            // 230
    uint64_t helmet;                                 // 301
    uint64_t cape;                                   // 309
    uint64_t neck;                                   // 317
    uint64_t weapon;                                 // 325
    uint64_t shield;                                 // 333
    uint64_t chest;                                  // 341
    uint64_t ringLeft;                               // 349
    uint64_t ringRight;                              // 357
    uint64_t boots;                                  // 365
    uint64_t arti1;                                  // 373
    uint64_t arti2;                                  // 381
    uint64_t arti3;                                  // 389
    uint64_t arti4;                                  // 397
    uint64_t balista;                                // 405
    uint64_t ammoCart;                               // 413
    uint64_t firstAidTent;                           // 421
    uint64_t catapult;                               // 429
    uint64_t spellBook;                              // 437
    uint64_t arti5;                                  // 445
    uint8_t padding7;                                // 453
    uint8_t helemtBlocked;                           // 454
    uint8_t capeBlocked;                             // 455
    uint8_t neckBlocked;                             // 456
    uint8_t weaponBlocked;                           // 457
    uint8_t shieldBlocked;                           // 458
    uint8_t chestBlocked;                            // 459
    uint8_t ringBlocked;                             // 460
    uint8_t bootsBlocked;                            // 461
    uint8_t artiBlocked;                             // 462
    uint8_t padding8[5];                             // 463
    uint64_t backpack[64];                           // 468
    uint8_t padding9[22];                            // 980
    uint8_t spells[140];                             // 1002
    uint8_t attackSkill;                             // 1142
    uint8_t defenceSkill;                            // 1143
    uint8_t powerSkill;                              // 1144
    uint8_t knowledgeSkill;                          // 1145
    uint8_t padding10[24];                           // 1146
};


class HeroMapper : public QObject
{
    Q_OBJECT
public:
    HeroMapper();

    /**
     * @brief init Sets the username to search for in the homm3 memory.
     * It also initiates the scanner to attach to homm3 and search for data.
     */

    void init();

    /**
     * @brief getHeroName Returns the name of the currently selected hero
     * @return The hero name which is displayed in the normal hero screen.
     * Returns "No hero name found" if unknown hero id is found
     */
    QString getHeroName();

    /**
     * @brief getHeroPotrait Returns a string with the file name of the portrait
     * for the current selected hero
     * @return The image name of the selected hero.
     */
    QString getHeroPotrait();

    /**
     * @brief getSP Returns the amount of spell power the hero has
     * @return The amount of spell power the current selected hero has left
     */
    QString getSP();

    /**
     * @brief getLevel Returns the hero level of the selected hero
     * @return The hero level of the seleceted hero
     */
    QString getLevel();

    /**
     * @brief getHelmet returns helmet data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the helmet slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getHelmet();

    /**
     * @brief getCape returns cape data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the cape slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getCape();

    /**
     * @brief getNeck returns neck data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the neck slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getNeck();

    /**
     * @brief getWeapon returns weapon data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the weapon slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getWeapon();

    /**
     * @brief getSheild returns sheild data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the shield slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getSheild();

    /**
     * @brief getChest returns weapon data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the weapon slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getChest();

    /**
     * @brief getRingLeft returns left ring data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the left ring slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getRingLeft();

    /**
     * @brief getRingRight returns right ring data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the right ring slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getRingRight();

    /**
     * @brief getBoots returns boots data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the boots slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getBoots();

    /**
     * @brief getArti1 returns arti data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the arti1 slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getArti0();

    /**
     * @brief getArti2 returns arti data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the arti2 slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getArti1();

    /**
     * @brief getArti3 returns arti data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the arti3 slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getArti2();

    /**
     * @brief getArti4 returns arti data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the arti4 slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getArti3();

    /**
     * @brief getArti5 returns arti data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the arti5 slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getArti4();

    /**
     * @brief getSpellBook returns spell book data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the spell book slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getSpellBook();

    /**
     * @brief getBalista returns balista data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the balista slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getBalista();

    /**
     * @brief getAmmoCart returns ammo cart data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the ammo cart slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getAmmoCart();

    /**
     * @brief getFirstAidTent returns first aid tent from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the first aid tent slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getFirstAidTent();

    /**
     * @brief getCatapult returns catapult data from the "hero buffer",
     * which is populated by the scanner.
     * @return The filename of the item in the catapult slot. Always returns
     * empty string if init has not been called yet.
     */
    QString getCatapult();

    /**
     * @brief getAttack returns the attack values from the "hero buffer",
     * which is populated by the scanner.
     * @return The attack value of the selected hero.
     */
    QString getAttack();
    /**
     * @brief getDefence returns the defence values from the "hero buffer",
     * which is populated by the scanner.
     * @return The defence value of the selected hero.
     */
    QString getDefence();
    /**
     * @brief getPower returns the power values from the "hero buffer",
     * which is populated by the scanner.
     * @return The power value of the selected hero.
     */
    QString getPower();
    /**
     * @brief getKnowledge returns the knowledge values from the "hero buffer",
     * which is populated by the scanner.
     * @return The attack knowledge of the selected hero.
     */
    QString getKnowledge();

    QString getSkillInSlot(const uint8_t &slotNumber);

    QString getSkill0();

    QString getSkill1();

    QString getSkill2();

    QString getSkill3();

    QString getSkill4();

    QString getSkill5();

    QString getSkill6();

    QString getSkill7();

    QString getCreature0();

    QString getCreature1();

    QString getCreature2();

    QString getCreature3();

    QString getCreature4();

    QString getCreature5();

    QString getCreature6();

    QString getCreatureAmount0();

    QString getCreatureAmount1();

    QString getCreatureAmount2();

    QString getCreatureAmount3();

    QString getCreatureAmount4();

    QString getCreatureAmount5();

    QString getCreatureAmount6();

    /**
     * @brief getMemoryScannerObject Returns a pointer to the scanner object
     * @return a pointer to the memory scanner object
     */
    MemoryScanner* getMemoryScannerObject();

private:

    /**
     * @brief isArtiSlotBlocked Function to figure out which arti slots are
     * blocked by combination artis.
     * @param artiSlot The asking arti (misc) slot
     * @return True if the given slot should be blocked by a combination arti
     */
    bool isArtiSlotBlocked(const uint8_t &artiSlot);

    /**
     * @brief mapValueToItem Converts memory values to artifact/spell names.
     * These names are mapped to the file name of the corresponding images.
     * @param itemValue The memory value of an item in a slot.
     * @return The filename of the corresponding item.
     */
    QString mapValueToItem(const uint64_t &itemValue);

    /**
     * @brief mapValueToMonster Converts memory values to monster names.
     * These names are mapped to the file name of the corresponding images.
     * @param monsterValue The memory value of a monster,
     * @return The filename of the corresponding monster.
     */
    QString mapValueToMonster(const uint32_t &monsterValue);

    /**
     * @brief creatureAmountToString Converts uint32_t value to QString
     * @param value The uint32_t value of the amount of creatures
     * @return The QString representation of the value. Returns empty string
     * if the value is 0.
     */
    QString creatureAmountToString(const uint32_t &value);

    MemoryScanner scanner; /*< Scanner which searches the homm3 memory */
    HeroStruct hero; /*< struct which mapps to hero memory in homm3 */

};

#endif // HEROMAPPER_H
