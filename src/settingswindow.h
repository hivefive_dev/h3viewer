#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsWindow(QWidget *parent = nullptr);
    ~SettingsWindow();


private:
    Ui::SettingsWindow *ui;

private slots:
    void includeDebug(bool isChecked);

signals:
    void includeDebugUpdated(bool activated);
};

#endif // SETTINGSWINDOW_H
