#ifndef MEMORYSCANNER_H
#define MEMORYSCANNER_H

#include <Windows.h>
#include <tlhelp32.h>

#include <QObject>
#include <QTimer>
#include <QString>


enum actionWindowState
{
    restore,
    itemPickup,
    newDay
};


class MemoryScanner : public QObject
{
    Q_OBJECT

public:
    MemoryScanner();
    /**
     * @brief init This will start the scanner to find the homm process,
     * attach to it and start the cyclic memory rading of the homm process. It
     * will put the result in the heroBuffer continuously. The caller is
     * responsible to allocate data for the heroBuffer.
     * @param heroBuffer A ponter where the scanner will dump the hero data to
     * @param bufferSize the size of the buffer. Should be at least
     * "HERO_ALLOCATION_SIZE" big
     * @return true if the buffer size is big enough
     */
    bool init(uint8_t *heroBuffer, const size_t &bufferSize);

    /**
     * @brief getHeroID Returns the hero id of the currently selected hero
     * if that hero is a "local player"
     * @return The id of the currently selected hero.
     */
    uint32_t getHeroID();

private slots:
    /**
     * @brief updateState Called cyclically based on the processCheckTimer.
     * It will update the current view of the hero state if possible.
     */
    void updateState();

private:
    /**
     * @brief getPointerValue Reads a memory location in the HoMMm assumes it is
     * a normal 32 bit pointer and returns the address of what it is pointing to.
     * @param address The address to read the pointer value from.
     * @return The address of what the pointer is pointing to.
     */
    uintptr_t getPointerValue(const uintptr_t &address);

    /**
     * @brief GetHommPid Searches the process list for h3hota.exe and h3hota HD.exe
     * @return The pid of the HoMM HotA PID.
     */
    DWORD GetHommPid();

    /**
     * @brief giveExtraPermissions Asks Windows for extra permissions. Required
     * to be allowed to attach to HoMM HotA.
     * @return True if successful, otherwise returns false.
     */
    bool giveExtraPermissions();

    /**
     * @brief attachToProcess Tries to attach to the the HoMM process.
     * @return True if successful, otherwise returns false.
     */
    bool attachToProcess();

    /**
     * @brief findSelectedHero Searches for the selected hero in the HoMM memory.
     * @return True if successful, otherwise returns false.
     */
    bool findSelectedHero();

    /**
     * @brief copySelectedHeroFromMemory Copies data from the HoMM process
     * to the heroBuffer.
     * @return True if successful, otherwise returns false.
     */
    bool copySelectedHeroFromMemory();

    /**
     * @brief checkForMapInteractions Check if the bottom right corner chages
     * to a pickup/interaction view, in that case this triggers a signal
     */
    void checkForMapInteractions();

    /** * * * * * * * * * *
     *  Member variables  *
     ** * * * * * * * * * */

    DWORD HommPID;
    HANDLE processHandler;
    QTimer pollRateTimer;
    QTimer processCheckTimer;

    uint32_t heroID;
    LPCVOID selectedHeroAddress;
    uint32_t dllBaseAddress;
    uint32_t h3BaseAddress;
    uint8_t * heroBuffer; /*< The caller of init should provide the allocation for this buffer */
    uint8_t * previosHeroBuffer; /*< Creates an extra buffer to compare new data with */

    uint8_t previousBRStatusID; /** BR = Bottom right */
    uint16_t previousInteractionID;

    bool gameIsSoD;

signals:
    void dataUpdated();
    void actionViewUpdated(actionWindowState state);
};

#endif // MEMORYSCANNER_H
