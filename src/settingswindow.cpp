#include "settingswindow.h"
#include "ui_settingswindow.h"

SettingsWindow::SettingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsWindow)
{
    ui->setupUi(this);

    connect(ui->includeDebugBox, &QCheckBox::clicked, this, &SettingsWindow::includeDebug);
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::includeDebug(bool isChecked)
{
    emit includeDebugUpdated(isChecked);
}
