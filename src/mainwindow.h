#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPoint>
#include <QSettings>
#include <QTimer>
#include <QLabel>
#include <QRect>

#include "heromapper.h"
#include "aboutwindow.h"
#include "settingswindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    /**
     * @brief refreshUI Fetches the current values of the heromapper and
     * converts it to images which are displayed in the main window.
     */
    void refreshUI();

    /**
     * @brief updateDebugStatus Toggles if debug messages should appear in the log.
     * @param activated If true, debug messages will appear
     */
    void updateDebugStatus(const bool &activated);

    /**
     * @brief clearModfiers clears + and - stat indication when changing items
     */
    void clearModfiers();

private:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *event);
    void exit();

    /**
     * @brief toggleView Switches between having window border/menu and only
     * displaying the main content.
     */
    void toggleView();

    /**
     * @brief readSettings Reads from the settings file and updates values
     * depending on what it reads.
     */
    void loadSettings();

    /**
     * @brief writeSettings Writes the settings file.
     */
    void saveSettings();

    /**
     * @brief initHeroMapper Starts the scanner.
     */
    void initHeroMapper();

    Ui::MainWindow *ui;
    HeroMapper hero;

    bool hideToggle;
    QPointF dragingOldWindowPos;
    bool holdingLeft;

    AboutWindow about;
    SettingsWindow settings;
    QSettings storedSettings;

    QRect previousGeometry;
    QString previosAttack;
    QString previosDefence;
    QString previosPower;
    QString previosKnowledge;
};
#endif // MAINWINDOW_H
