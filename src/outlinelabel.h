#ifndef OUTLINELABEL_H
#define OUTLINELABEL_H

#include <QLabel>
#include <QPaintEvent>
#include <QPen>
#include <QPainterPath>
#include <QFont>
#include <QPainter>

class OutlineLabel : public QLabel
{
public:
    OutlineLabel(QWidget *parent);

private:
    QPainterPath outlinePath;
    QPen outlinePen;
    QFont outlineFont;
    bool once;

protected:
    void paintEvent(QPaintEvent* event);
};

#endif // OUTLINELABEL_H
