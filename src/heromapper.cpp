#include "heromapper.h"

#include <QDebug>

#define SPELLBOOK_FILE "Artifact_Spell_Book.png"
#define SLOT_BLOCKED_IMG "Artifact_Blocked.png"
#define NONE "None"

HeroMapper::HeroMapper()
{
    memset(&(this->hero), 0, sizeof(HeroStruct));
}

void HeroMapper::init()
{
    memset(&hero, 0, sizeof(HeroStruct));
    this->scanner.init((uint8_t*)&(this->hero), sizeof(HeroStruct));
}

QString HeroMapper::getHeroName()
{
    if(heroMap.contains(this->scanner.getHeroID()))
    {
        return heroMap.value(this->scanner.getHeroID());
    }
    return QString("No hero name found");
}

QString HeroMapper::getHeroPotrait()
{
    if(heroMap.contains(this->scanner.getHeroID()))
    {
        return QString("Hero_" + QString(QString::number(this->scanner.getHeroID()) + ".png"));
    }
    return QString(NONE);
}

QString HeroMapper::getSP()
{
    return QString::number(this->hero.mana);
}

QString HeroMapper::getLevel()
{
    return QString::number(this->hero.level);
}

QString HeroMapper::getHelmet()
{
    QString itemInSlot = mapValueToItem(this->hero.helmet);

    // Check if slot is blocked by combi arti
    if (this->hero.helemtBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }

    return itemInSlot;
}

QString HeroMapper::getCape()
{
    QString itemInSlot = mapValueToItem(this->hero.cape);

    // Check if slot is blocked by combi arti
    if (this->hero.capeBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getNeck()
{
    QString itemInSlot =mapValueToItem(this->hero.neck);

    // Check if slot is blocked by combi arti
    if (this->hero.neckBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getWeapon()
{
    QString itemInSlot = mapValueToItem(this->hero.weapon);

    // Check if slot is blocked by combi arti
    if (this->hero.weaponBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getSheild()
{
    QString itemInSlot = mapValueToItem(this->hero.shield);

    // Check if slot is blocked by combi arti
    if (this->hero.shieldBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getChest()
{
    QString itemInSlot = mapValueToItem(this->hero.chest);

    // Check if slot is blocked by combi arti
    if (this->hero.chestBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getRingLeft()
{
    QString itemInSlot = mapValueToItem(this->hero.ringLeft);

    // Check if slot is blocked by combi arti
    if (itemInSlot == NONE &&
            ((this->hero.ringBlocked > 0 &&
             mapValueToItem(this->hero.ringRight) != NONE) ||
            this->hero.ringBlocked == 2))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getRingRight()
{
    QString itemInSlot = mapValueToItem(this->hero.ringRight);

    // Check if slot is blocked by combi arti
    if (itemInSlot == NONE && this->hero.ringBlocked > 0)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getBoots()
{
    QString itemInSlot = mapValueToItem(this->hero.boots);

    // Check if slot is blocked by combi arti
    if (this->hero.bootsBlocked)
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

bool HeroMapper::isArtiSlotBlocked(const uint8_t &artiSlot)
{
    uint8_t numberOfBlockedArti = this->hero.artiBlocked;
    uint8_t consumedArtiSlots = 0;

    if(mapValueToItem(this->hero.arti5) == NONE && numberOfBlockedArti > consumedArtiSlots)
    {
        consumedArtiSlots++;
        if (artiSlot == 5)
        {
            return true;
        }
    }
    if(mapValueToItem(this->hero.arti4) == NONE && numberOfBlockedArti > consumedArtiSlots)
    {
        consumedArtiSlots++;
        if (artiSlot == 4)
        {
            return true;
        }
    }
    if(mapValueToItem(this->hero.arti3) == NONE && numberOfBlockedArti > consumedArtiSlots)
    {
        consumedArtiSlots++;
        if (artiSlot == 3)
        {
            return true;
        }
    }
    if(mapValueToItem(this->hero.arti2) == NONE && numberOfBlockedArti > consumedArtiSlots)
    {
        consumedArtiSlots++;
        if (artiSlot == 2)
        {
            return true;
        }
    }
    if(mapValueToItem(this->hero.arti1) == NONE && numberOfBlockedArti > consumedArtiSlots)
    {
        consumedArtiSlots++;
        if (artiSlot == 1)
        {
            return true;
        }
    }
    return false;
}

QString HeroMapper::getArti0()
{
    QString itemInSlot = mapValueToItem(this->hero.arti1);
    if (isArtiSlotBlocked(1))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getArti1()
{
    QString itemInSlot = mapValueToItem(this->hero.arti2);
    if (isArtiSlotBlocked(2))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getArti2()
{
    QString itemInSlot = mapValueToItem(this->hero.arti3);
    if (isArtiSlotBlocked(3))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getArti3()
{
    QString itemInSlot = mapValueToItem(this->hero.arti4);
    if (isArtiSlotBlocked(4))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getArti4()
{
    QString itemInSlot = mapValueToItem(this->hero.arti5);
    if (isArtiSlotBlocked(5))
    {
        itemInSlot = QString(SLOT_BLOCKED_IMG);
    }
    return itemInSlot;
}

QString HeroMapper::getSpellBook()
{
    return mapValueToItem(this->hero.spellBook);
}

QString HeroMapper::getBalista()
{
    return mapValueToItem(this->hero.balista);
}

QString HeroMapper::getAmmoCart()
{
    return mapValueToItem(this->hero.ammoCart);
}

QString HeroMapper::getFirstAidTent()
{
    return mapValueToItem(this->hero.firstAidTent);
}

QString HeroMapper::getCatapult()
{
    return mapValueToItem(this->hero.catapult);
}

QString HeroMapper::getAttack()
{
    uint8_t attack = this->hero.attackSkill;

    if ((attack > 200) && (attack <= 255)) {
        attack = 0;
    } else if (attack > 99) {
        attack = 99;
    }

    return QString::number(attack);
}

QString HeroMapper::getDefence()
{
    uint8_t defence = this->hero.defenceSkill;

    if ((defence > 200) && (defence <= 255)) {
        defence = 0;
    } else if (defence > 99) {
        defence = 99;
    }

    return QString::number(defence);
}

QString HeroMapper::getPower()
{
    uint8_t power = this->hero.powerSkill;

    if ((power > 200) && (power <= 255)) {
        power = 1;
    } else if (power > 99) {
        power = 99;
    }

    return QString::number(power);
}

QString HeroMapper::getKnowledge()
{
    uint8_t knowledge = this->hero.knowledgeSkill;

    if ((knowledge > 200) && (knowledge <= 255)) {
        knowledge = 1;
    } else if (knowledge > 99) {
        knowledge = 99;
    }

    return QString::number(knowledge);
}

QString valueToSkillLevel(const uint8_t &value)
{
    switch(value)
    {
    case 1:
        return QString("Basic");
        break;
    case 2:
        return QString("Advanced");
        break;
    case 3:
        return QString("Expert");
        break;
    }
    return QString("");
}

QString HeroMapper::getSkillInSlot(const uint8_t &slotNumber)
{
    if (slotNumber > 7)
    {
        qWarning() << "Only slot 0-7 are possible.";
        return QString("");
    }

    uint8_t passedActiveSkills = 0;
    uint8_t* skill = &(this->hero.pathfindingSkill);

    for(int i = 0; i < skillList.length(); i++)
    {
        // if skill > 0, the skill have been picked
        if(*skill > 0)
        {
            if(slotNumber == passedActiveSkills)
            {
                return "Skill_" + valueToSkillLevel(*skill) + "_" + skillList.at(i);
            }
            passedActiveSkills++;
        }
        skill++;
    }
    return QString("");
}

QString HeroMapper::getSkill0()
{
    return getSkillInSlot(0);
}

QString HeroMapper::getSkill1()
{
    return getSkillInSlot(1);
}

QString HeroMapper::getSkill2()
{
    return getSkillInSlot(2);
}

QString HeroMapper::getSkill3()
{
    return getSkillInSlot(3);
}

QString HeroMapper::getSkill4()
{
    return getSkillInSlot(4);
}

QString HeroMapper::getSkill5()
{
    return getSkillInSlot(5);
}

QString HeroMapper::getSkill6()
{
    return getSkillInSlot(6);
}

QString HeroMapper::getSkill7()
{
    return getSkillInSlot(7);
}

QString HeroMapper::getCreature0()
{
    return mapValueToMonster(this->hero.creatureIDInSlot0);
}

QString HeroMapper::getCreature1()
{
    return mapValueToMonster(this->hero.creatureIDInSlot1);
}

QString HeroMapper::getCreature2()
{
    return mapValueToMonster(this->hero.creatureIDInSlot2);
}

QString HeroMapper::getCreature3()
{
    return mapValueToMonster(this->hero.creatureIDInSlot3);
}

QString HeroMapper::getCreature4()
{
    return mapValueToMonster(this->hero.creatureIDInSlot4);
}

QString HeroMapper::getCreature5()
{
    return mapValueToMonster(this->hero.creatureIDInSlot5);
}

QString HeroMapper::getCreature6()
{
    return mapValueToMonster(this->hero.creatureIDInSlot6);
}

QString HeroMapper::creatureAmountToString(const uint32_t &value)
{
    if (value == 0)
    {
        return QString("");
    }
    return QString::number(value);
}

QString HeroMapper::getCreatureAmount0()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot0);
}

QString HeroMapper::getCreatureAmount1()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot1);
}

QString HeroMapper::getCreatureAmount2()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot2);
}

QString HeroMapper::getCreatureAmount3()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot3);
}

QString HeroMapper::getCreatureAmount4()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot4);
}

QString HeroMapper::getCreatureAmount5()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot5);
}

QString HeroMapper::getCreatureAmount6()
{
    return creatureAmountToString(this->hero.creatureAmountInSlot6);
}

MemoryScanner *HeroMapper::getMemoryScannerObject()
{
    return &(this->scanner);
}

QString HeroMapper::mapValueToItem(const uint64_t &itemValue)
{
    if (itemMap.contains(itemValue))
    {
        return itemMap[itemValue];
    }
    else if (itemValue == 0xFFFFFFFFFFFFFFFF)
    {
        return QString(NONE);
    }
    else if(itemValue == 0)
    {
        return QString("");
    }
    else
    {
        return QString("Unknown item");
    }
}


QString HeroMapper::mapValueToMonster(const uint32_t &monsterValue)
{
    if (monsterMap.contains(monsterValue))
    {
        return monsterMap[monsterValue];
    }
    else if (monsterValue == 0xFFFFFFFF)
    {
        return QString(NONE);
    }
    else if(monsterValue == 0)
    {
        return QString("");
    }
    else
    {
        return QString("Unknown Monster");
    }
}
