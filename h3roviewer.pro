QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

RC_ICONS = resources/images/icon.ico

VERSION = 1.0.2.0

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/outlinelabel.cpp \
    src/aboutwindow.cpp \
    src/heromapper.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/memoryscanner.cpp \
    src/settingswindow.cpp

HEADERS += \
    src/outlinelabel.h \
    src/aboutwindow.h \
    src/heromapper.h \
    src/imagesMap.h \
    src/mainwindow.h \
    src/memoryscanner.h \
    src/settingswindow.h

FORMS += \
    forms/mainwindow.ui \
    forms/aboutwindow.ui \
    forms/settingswindow.ui

TRANSLATIONS += \
    H3roViewer_en_150.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/resources.qrc



copyManifest.commands = $(COPY_FILE) $$shell_path($$PWD\resources\h3roviewer.exe.manifest)  $$shell_path($$OUT_PWD/release/)
copyReadme.commands = $(COPY_FILE) $$shell_path($$PWD\README.md) $$shell_path($$OUT_PWD/release/)
copyLicense.commands = $(COPY_FILE) $$shell_path($$PWD\LICENSE.md) $$shell_path($$OUT_PWD/release/)
first.depends = $(first) copyManifest copyReadme copyLicense
export(first.depends)
export(copyManifest.commands)
export(copyReadme.commands)
export(copyLicense.commands)
QMAKE_EXTRA_TARGETS += first copyManifest copyReadme copyLicense
