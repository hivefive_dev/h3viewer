## USAGE
This application requires elevated permissions to attach to Heroes 3 HotA.  
Simply start this application and it should automatically try to attach to Heroes 3 HotA.  
It will look for a process named "h3hota.exe" or "h3hota HD.exe".   
If the process is named differently, it will not work.  
As soon as a game is active, the selected hero should appear in the main window.



## BUILDING
This was compiled using QtCreator 6.0.0, Qt 6.2.2, MinGW 9.0.0, 64 bit.  

Get Qt here: https://www.qt.io/download-open-source.  
The default installation should include MinGW.  
Open QtCreator and load the project file "h3roviewer.pro"  
Compile.

To make a standalone package, the binary need some libraries in the same directory as the exe.  
This can be done with the "windeployqt.exe" which is in the \<Qt installation>/mingw_64/bin/  
```
  $ windeployqt.exe <path to the directory where the compiled exe is>
```
It must have the same build environment as when compiling.  
Can be added as a custom build step in QtCreator.